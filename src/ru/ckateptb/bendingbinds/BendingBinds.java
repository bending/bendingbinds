package ru.ckateptb.bendingbinds;

import com.projectkorra.projectkorra.Element;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import ru.ckateptb.bendingbinds.command.GuiCommand;
import ru.ckateptb.bendingbinds.nms.INMSManager;
import ru.ckateptb.bendingbinds.nms.NMSManager_111R1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BendingBinds extends JavaPlugin {
    public static boolean pageArrowMoveMouse = false;

    public static boolean enableOfflinePlayers = false;

    public static Logger log;

    public static BendingBinds INSTANCE;

    public static boolean loaded = false;
    public static boolean enabled = true;

    public static List<Element> elementOrder;

    public static List<String> getDescriptions(String description, ChatColor color, int length) {
        Pattern p = Pattern.compile("\\G\\s*(.{1," + length + "})(?=\\s|$)", Pattern.DOTALL);
        Matcher m = p.matcher(description);
        List<String> l = new ArrayList<String>();
        while (m.find()) {
            l.add(color + m.group(1));
        }
        return l;
    }

    public static INMSManager getNMSManager() {
        return new NMSManager_111R1();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!loaded || !this.isEnabled()) return false;
        if (command.getName().equalsIgnoreCase("bendingbinds")) {
            return GuiCommand.executeCommand(sender, Arrays.asList(args));
        }
        return false;
    }

    @Override
    public void onEnable() {
        INSTANCE = this;
        log = getLogger();
        getServer().getPluginManager().registerEvents(new Listener(), this);
        if (Bukkit.getPluginManager().getPlugin("ProjectKorra") == null || !Bukkit.getPluginManager().getPlugin("ProjectKorra").isEnabled()) {
            log.severe("ProjectKorra plugin not installed! This plugin is completely useless without it!");
            this.setEnabled(false);
            return;
        }

        Config.load();
        Descriptions.load();
        Descriptions.save();
        loadElementOrder();
        new GuiCommand();

        log.log(Level.INFO, enabled ? "BendingBinds Fully Loaded!" : "BendingBinds loaded but functional.");

        loaded = true;
    }

    public void loadElementOrder() {
        elementOrder = new ArrayList<Element>();

        elementOrder.add(Element.AIR);
        elementOrder.add(Element.FLIGHT);
        elementOrder.add(Element.SPIRITUAL);
        if (Element.getAddonSubElements(Element.AIR).length > 0)
            elementOrder.addAll(Arrays.asList(Element.getAddonSubElements(Element.AIR)));
        elementOrder.add(Element.EARTH);
        elementOrder.add(Element.SAND);
        elementOrder.add(Element.METAL);
        elementOrder.add(Element.LAVA);
        if (Element.getAddonSubElements(Element.EARTH).length > 0)
            elementOrder.addAll(Arrays.asList(Element.getAddonSubElements(Element.EARTH)));
        elementOrder.add(Element.FIRE);
        elementOrder.add(Element.LIGHTNING);
        elementOrder.add(Element.COMBUSTION);
        if (Element.getAddonSubElements(Element.FIRE).length > 0)
            elementOrder.addAll(Arrays.asList(Element.getAddonSubElements(Element.FIRE)));
        elementOrder.add(Element.WATER);
        elementOrder.add(Element.ICE);
        elementOrder.add(Element.HEALING);
        elementOrder.add(Element.PLANT);
        elementOrder.add(Element.BLOOD);
        if (Element.getAddonSubElements(Element.WATER).length > 0)
            elementOrder.addAll(Arrays.asList(Element.getAddonSubElements(Element.WATER)));
        elementOrder.add(Element.CHI);
        if (Element.getAddonSubElements(Element.CHI).length > 0)
            elementOrder.addAll(Arrays.asList(Element.getAddonSubElements(Element.CHI)));

        for (Element e : Element.getAddonElements()) {
            if (e == null) continue;
            elementOrder.add(e);
            if (Element.getAddonSubElements(e).length > 0)
                elementOrder.addAll(Arrays.asList(Element.getAddonSubElements(e)));
        }

        elementOrder.add(Element.AVATAR);
    }

    public void reload() {
        Config.load();
        Descriptions.load();
        Descriptions.save();
        log.log(Level.INFO, "BendingBinds Reloaded!");
    }

    public String makeListFancy(List<? extends Object> list) {
        String s = "";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                s = list.get(0).toString();
            } else if (i == list.size() - 1) {
                s = s + " and " + list.get(i);
            } else {
                s = s + ", " + list.get(i);
            }
        }
        return s;
    }
}
