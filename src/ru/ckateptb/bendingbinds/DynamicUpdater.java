package ru.ckateptb.bendingbinds;

import ru.ckateptb.bendingbinds.menus.MenuBendingOptions;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DynamicUpdater {
    /**
     * Standard GUI
     */
    protected static HashMap<UUID, MenuBendingOptions> guiData = new HashMap<UUID, MenuBendingOptions>();
    protected static HashMap<UUID, List<UUID>> players = new HashMap<UUID, List<UUID>>();
    protected static HashMap<UUID, Integer> pages = new HashMap<UUID, Integer>();

    /**
     * Gets the menu for the player given.
     */
    public static MenuBendingOptions getMenu(OfflinePlayer player) {
        if (guiData.keySet().contains(player.getUniqueId())) {
            return guiData.get(player.getUniqueId());
        }
        return null;
    }

    /**
     * Update everyone elses Menu with the one provided.
     */
    public static void updateMenu(OfflinePlayer player, MenuBendingOptions menu) {
        guiData.put(player.getUniqueId(), menu);
        if (!players.containsKey(player.getUniqueId())) {
            players.put(player.getUniqueId(), new ArrayList<UUID>());
        }
        for (UUID id : players.get(player.getUniqueId())) {
            if (Bukkit.getPlayer(id) == null) {
                players.get(player.getUniqueId()).remove(id);
                continue;
            }
            if (Bukkit.getPlayer(id).getOpenInventory() == null || Bukkit.getPlayer(id).getOpenInventory().getTopInventory() == null || !(Bukkit.getPlayer(id).getOpenInventory().getTopInventory() instanceof MenuBendingOptions)) {
                players.get(player.getUniqueId()).remove(id);
                continue;
            }

            if (((MenuBendingOptions) Bukkit.getPlayer(id).getOpenInventory().getTopInventory()).getMenuPlayer().getUniqueId() == player.getUniqueId()) {
                continue;
            }

            if (Bukkit.getPlayer(player.getUniqueId()) == menu.getOpenPlayer()) continue;

            MenuBendingOptions menu1 = (MenuBendingOptions) Bukkit.getPlayer(id).getOpenInventory().getTopInventory();
            menu1.updateFromMenu(menu);
        }
    }

    public static void setPage(OfflinePlayer player, int page) {
        pages.put(player.getUniqueId(), page);
    }

    public static int getPage(OfflinePlayer player) {
        if (pages.containsKey(player.getUniqueId()))
            return pages.get(player.getUniqueId());
        return 0;
    }

}
