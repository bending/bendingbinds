package ru.ckateptb.bendingbinds;

import ru.ckateptb.bendingbinds.menus.MenuBendingOptions;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

public class Listener implements org.bukkit.event.Listener {
    @EventHandler(priority = EventPriority.LOW)
    public void onMenuItemClicked(InventoryClickEvent event) {
        if (!BendingBinds.enabled || event.isCancelled()) return;
        try {
            Inventory inventory = event.getInventory();
            if (inventory.getHolder() instanceof MenuBase) {
                MenuBase menu = (MenuBase) inventory.getHolder();
                if (event.getWhoClicked() instanceof Player) {
                    Player player = (Player) event.getWhoClicked();
                    if (event.getSlotType() != InventoryType.SlotType.OUTSIDE) {
                        int index = event.getRawSlot();
                        if (index < inventory.getSize()) {
                            if (event.getCursor().getType() != Material.AIR) {
                                event.setCancelled(true);
                                menu.closeMenu(player);
                                player.sendMessage(ChatColor.RED + "The bending gui cannot be tampered with!");
                            }
                            MenuItem item = menu.getMenuItem(index);
                            if (item != null) {
                                item.onClick(player);
                                event.setCancelled(true);
                            }
                        } else {
                            if (event.isShiftClick()) {
                                event.setCancelled(true);
                            }
                            menu.setLastClickedSlot(index);
                        }
                    }
                }
            }
        } catch (Exception e) {
            event.getWhoClicked().closeInventory();
            event.getWhoClicked().sendMessage(ChatColor.RED + "An error occured while processing the clickevent. Please report this to your admin or the plugin developer!");
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (!BendingBinds.enabled) return;
        if (e.getInventory() instanceof MenuBendingOptions) {
            MenuBendingOptions menu = (MenuBendingOptions) e.getInventory();
            if (DynamicUpdater.players.containsKey(menu.getMenuPlayer().getUniqueId()) && DynamicUpdater.players.get(menu.getOpenPlayer().getUniqueId()).contains(menu.getOpenPlayer())) {
                DynamicUpdater.players.get(menu.getOpenPlayer().getUniqueId()).remove(menu.getOpenPlayer());
            }
        }
    }
}
