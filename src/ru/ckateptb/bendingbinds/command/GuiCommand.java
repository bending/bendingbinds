package ru.ckateptb.bendingbinds.command;

import com.projectkorra.projectkorra.command.PKCommand;
import ru.ckateptb.bendingbinds.menus.MenuBendingOptions;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class GuiCommand extends PKCommand {
    public GuiCommand() {
        super("BendingBinds", "/binds", "Open the binds menu", new String[]{"bind", "binds", "bendingbind", "b"});
    }

    public static boolean executeCommand(CommandSender sender, List<String> args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only players can run this command!");
            return true;
        }
        if (!sender.hasPermission("bending.player")) {
            sender.sendMessage(ChatColor.RED + "You don't have permission to use this command!");
            return true;
        }
        Player player = (Player) sender;
        MenuBendingOptions menu = new MenuBendingOptions(player);
        menu.openMenu(player);
        return true;
    }

    public void execute(CommandSender arg0, List<String> arg1) {
        executeCommand(arg0, arg1);
    }
}
